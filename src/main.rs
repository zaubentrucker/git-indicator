#[derive(Debug)]
enum Status {
    Clean,
    ChangesAvailable,
    ErrorOccured(String),
}

#[derive(Debug)]
struct GitStatusTray {
    status: Status,
}

impl ksni::Tray for GitStatusTray {
    fn icon_name(&self) -> String {
        match self.status {
            Status::ChangesAvailable => "user-available".into(),
            Status::Clean => "".into(),
            Status::ErrorOccured(_) => "dialog-error".into(),
        }
    }
    fn title(&self) -> String {
        "VCSH repo status".into()
    }
    fn menu(&self) -> Vec<ksni::MenuItem<Self>> {
        Vec::new()
    }
}

fn main() {
    std::env::set_current_dir(std::env::var("HOME").unwrap()).unwrap();
    let service = ksni::TrayService::new(GitStatusTray {
        status: Status::Clean,
    });
    let handle = service.handle();
    service.spawn();

    loop {
        std::thread::sleep(std::time::Duration::from_secs(3));

        let home = std::env::var("HOME").expect("$HOME is not available!");
        let vcsh_output: Result<String, &'static str> = std::process::Command::new("vcsh")
            .arg("notes")
            .arg("add")
            .arg(home + "/.notable")
            .output()
            .map_err(|_| "Failed to add notable changes!")
            .and_then(|_| {
                std::process::Command::new("vcsh")
                    .arg("status")
                    .output()
                    .map_err(|_| "Failed to call vcsh!")
            })
            .map(|out| out.stdout)
            .and_then(|stdout| {
                String::from_utf8(stdout).map_err(|_| "vcsh produced invalid UTF8!")
            });

        let has_changed: Result<bool, &'static str> = vcsh_output
            .map(|stdout_string| {
                stdout_string
                    .split("\n\n")
                    .map(|repo| repo.split('\n').count())
                    .collect()
            })
            .map(|lens: Vec<_>| lens.into_iter().any(|len| len > 1));

        let _ = dbg!(has_changed);

        handle.update(|tray: &mut GitStatusTray| {
            tray.status = match has_changed {
                Ok(true) => Status::ChangesAvailable,
                Ok(false) => Status::Clean,
                Err(message) => Status::ErrorOccured(message.into()),
            };
        });
    }
}
